let user = {
    name: "Nirav",
    address: {
        city: "Bhuj"
    }
};

// let user1 = user;
// user1.name = "Rahul";

//First way shallow copy
let user1 = Object.assign({}, user);
user1.name = "Rahul";

//Second way shallow copy
let user1 = { ...user };
user1.name = "Rahul";
user1.address.city = "Gandhinagar"

//Deep copy
//First way deep copy(ES6)
let user1 = JSON.parse(JSON.stringify(user));
user1.name = "Rahul"
user1.address.city = "Gandhinagar";

//Second way of deep copy(Latest)
const clonnedOject = structuredClone(user)
clonnedOject.address.city = "Gandhinagar"

console.log("User", user1)
console.log("Clone object", clonnedOject);